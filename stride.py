#!/usr/bin/python3

loop = 1
upper_or_lower = 1
new_str = ''

usr_str = input('Enter a string: ')
stride = int(input('Enter a stride: '))

for char in usr_str:
    if upper_or_lower == 1:
       new_str += char.upper()
    else:
       new_str += char.lower()
    if loop % stride == 0:
        upper_or_lower *= -1
    loop += 1

print(new_str)
