#!/usr/bin/python3

unique_list = []
items = input('Enter a list of items: ')
items_list = items.split(' ')
for item in items_list:
    if item not in unique_list:
        unique_list.append(item)
print(unique_list)
