#!/usr/bin/python3

solution = {}
word_list = []

def build_word_list(length):
    with open ("words.full", "r") as data:
        for line in data:
            line = line.strip()
            if len(line) == length:
                word_list.append(line) 
    data.close()
    return

def mutate(key, last):
    mutations = []
    for word in word_list:
        index = 0
        char_match_key = 0
        char_match_last = 0
        for char in word:
            if char == key[index]:
                char_match_key += 1
            if char == last[index]:
                char_match_last += 1
            index += 1
        if (char_match_key == len(word) - 1) and (char_match_last > 0):
            if word not in mutations:
                mutations.append(word)
    return mutations


def build_chain(first, last):
    if len(fw) == len(lw):
        build_word_list(len(fw))
    else:
        print('Word lengths do not match.')
        return

    # get mutations for first word in chain
    mutations = mutate(first, last)
    # add first word and mutation list to solution dictionary
    solution[first] = mutations

    # mutate until last word in chain returns in mutation result set
    next_mutations = []
    while last not in mutations:
        # mutate each mutation
        for mutation in mutations:
            # if mutation does not exist in solution dictionary
            if mutation not in solution:
                mutations = mutate(mutation, last)
                solution[mutation] = mutations
                # build list of next words to mutate
                for next_mutation in mutations:
                    if (next_mutation not in next_mutations) and (next_mutation not in list(solution)):
                        next_mutations.append(next_mutation)
                mutations = next_mutations        
    return


#fw = input('Enter the first word: ')
#lw = input('Enter the last word: ')
fw = 'cat'
lw = 'dog'
build_chain(fw, lw)

for key, value in solution.items():
    print('key =', key, 'value =', value)
