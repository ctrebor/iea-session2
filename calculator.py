#!/usr/bin/python3
import math

def calculate(x, y, operator):
    result = None
    if operator == '+':
        result = x + y
    elif operator == '-':
        result = x - y
    elif operator == '*':
        result = x * y
    elif operator == '/':
        result = x / y
    elif operator == '//':
        result = x // y
    elif operator == '**':
        result = x ** y
    elif operator == 'log':
        try:
            result = math.log(x)/math.log(y)
        except ValueError:
            print('Invalid input for log fuction. Values must be > 0.')
        else:
            print('Input is valid.')
    else:
        print('Invalid operator.')

    return result
