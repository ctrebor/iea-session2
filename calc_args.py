#!/usr/bin/python3
import math
import sys

def calculate(x, y, operator):
    result = None
    if operator == '+':
        result = x + y
    elif operator == '-':
        result = x - y
    elif operator == '*':
        result = x * y
    elif operator == '/':
        result = x / y
    elif operator == '//':
        result = x // y
    elif operator == '**':
        result = x ** y
    elif operator == 'log':
        try:
            result = math.log(x)/math.log(y)
        except ValueError:
            print('Invalid input for log fuction. Values must be > 0.')
        else:
            print('Input is valid.')
    else:
        print('Invalid operator.')

    return result

if __name__ == '__main__':
    print('Program arguements ', sys.argv)
    if len(sys.argv) != 4:
        print('Script argument count not expected.')
    else:
        in1 = int(sys.argv[1])
        in2 = int(sys.argv[2])
        in3 = str(sys.argv[3])
        print(calculate(in1, in2, in3))
else:
    print('Imported!')
