from calc import calc

def test_sum():
    assert calc(1, 2, '+') == 3
def test_diff():
    assert calc(3, 2, '-') == 1
def test_product():
    assert calc(1, 2, '*') == 2
def test_quotient():
    assert calc(1, 2, '/') == 0.5
def test_pow():
    assert calc(3, 3, '**') == 27
def test_intdiv():
    assert calc(3, 2, '//') == 1
