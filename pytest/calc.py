#!/usr/bin/python3

def calc(x, y, operator):
    if operator == '+':
        return x+y
    elif operator == '-':
        return x-y
    elif operator == '*':
        return x*y
    elif operator == '/':
        return x/y
    elif operator == '//':
        return x//y
    elif operator == '**':
        return x**y
    else:
        print('Invalid input')
        return None

if __name__ == '__main__':
    x = int(input('Enter a number: '))
    y = int(input('Enter another number: '))
    operator = input('Enter an operator: ')
    result = calc(x, y, operator)
    print(result)
