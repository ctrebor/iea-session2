#! /usr/bin/env python3
import requests

DARK_SKY_SECRET_KEY="d21b17405d692b8977dd9098c59754eb"

def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
     """
    r = requests.get('https://ipvigilante.com/')
    data = r.json()
    latitude = data["data"]["latitude"]
    longitude = data["data"]["longitude"]
    print('lat =', latitude, 'long =', longitude)
    return latitude, longitude

def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    url = 'https://api.darksky.net/forecast/{key}/{latitude},{longitude}'.format(key=DARK_SKY_SECRET_KEY, latitude=latitude, longitude=longitude)
    r = requests.get(url)
    data = r.json()
    temp = data["currently"]["temperature"]
    print('temp =', temp)
    return temp

def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("Today's forecast is: ", temp, "degrees!")

if __name__ == "__main__":
    longitude, latitude = get_location()
    temp = get_temperature(longitude, latitude)
    print_forecast(temp)
