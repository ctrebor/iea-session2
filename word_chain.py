#!/usr/bin/python3

solution = {}

def get_mutations(mutation_list):
    mutations = []
    # iterate through items in mutation_list
    for item in mutation_list:
        # iterate through word file to find mutations of current item
        for word in open('words.simple'):
            word = word.rstrip()
            index = 0
            char_match = 0
            if len(word) == len(item):
                for char in word:
                    if char == item[index]:
                        char_match += 1
                    index += 1
                if char_match == len(word) - 1:
                    if word not in mutations:
                        mutations.append(word)
        if item not in solution:
            solution[item] = mutations
    return mutations

def build_dict(first, last):
    mutations = []
    mutations.append(first)
    while last not in mutations:
        mutations = get_mutations(mutations)
    # add last to solution dictionary
    mutations = []
    mutations.append(last)
    get_mutations(mutations)
    return

def print_chain(first, last):
    build_dict(first, last)
    # for demonstration purposes only
    print('Below are the contents of the solution dictionary: ')
    for key, value in solution.items():
        print('key =', key, 'value =', value)
    chain = []
    #chain.append(last)

    return chain
    
first_word = input('Enter the first word of the chain: ')
last_word = input('Enter the last word of the chain: ')
chain = print_chain(first_word, last_word)
print('word chain =', chain)
