#!/usr/bin/python3

import sys

solution = []
word_list = []
matches = []

def build_word_list(length):
    with open ("words", "r") as data:
        for line in data:
            line = line.strip()
            if len(line) == length:
                word_list.append(line) 
    data.close()
    return

def mutate(key, last):
    mutations = []
    for word in word_list:
        index = 0
        char_match_key = 0
        char_match_last = 0
        for char in word:
            if char == key[index]:
                char_match_key += 1
            if char == last[index]:
                char_match_last += 1
            index += 1
        if char_match_key == len(word) - 1:
            if (word not in mutations) and (word not in matches):
                if char_match_last > 0:
                    mutations.append(word)
    return mutations


def build_chain(first, last):
    if len(first) == len(last):
        build_word_list(len(first))
    else:
        print('Word lengths do not match.')
        return

    mutations = []
    next_mutations = []
    temp_mutations = []
    mutations = mutate(first, last)
    solution.append(mutations)
    while last not in mutations:
        for mutation in mutations:
            temp_mutations = mutate(mutation, last)
            for each in temp_mutations:
                if each not in next_mutations:
                    next_mutations.append(each)
        solution.append(next_mutations) 
        matches.extend(next_mutations)               
        mutations = next_mutations
        if len(mutations) == 0:
            print('No solution found.')
            break



    print(solution)
    print(len(solution))
    chain = get_chain()
    return chain
    
def get_chain():
    # use solution list to build chain
    # we know solution[0] and solution[-1]
    chain = []
    chain.append(solution[0])
    for index in range(1, len(solution)):
        if index == 1:
            for each in solution[index]:
                if each in solution[index + 1]:
                    chain.append(each)
        elif index == len(solution) - 1:
            for each in solution[index]:
                if each in solution[index - 1]:
                    chain.append(each)
        else:
            for each in solution[index]:
                if each in solution[index - 1] and each in solution[index + 1]:
                    chain.append(each)
    chain.append(solution[-1])
    return chain

if len(sys.argv) == 3:
    fw = str(sys.argv[1])
    lw = str(sys.argv[2])
else:
    fw = input('Enter the first word: ')
    lw = input('Enter the last word: ')
    
word_chain = build_chain(fw, lw)
print(word_chain)
