#!/usr/bin/python3

import sys

# consists of all words with same length of first & last
word_list = []
# consists of mutations of words in word_list that match with character(s) in first and list words in chain
full_dictionary = {}


def build_data_structures(first, last):
    with open("words", "r") as data:
        for line in data:
            index = 0        
            line = line.lower().strip()
            if len(line) == len(first):
                for char in line:
                    if line[index] in (first[index], last[index]) and line not in word_list:
                        word_list.append(line)
                    index += 1
    data.close()
    for word in word_list:
        mutations = mutate(word)
        if mutations:
            full_dictionary[word] = mutations
    return


def mutate(key):
    mutations = []
    for word in word_list:
        index = 0
        char_match = 0
        for char in word:
            if char == key[index]:
                char_match += 1
            index += 1
        if char_match == len(word) - 1:
            if word not in mutations:
                mutations.append(word)
    return mutations

    
def build_possibility_list(first, last):
    possibility_list = [[first], [last]]
    begin_to_end_ix = 0
    end_to_begin_ix = -1
    flip = 1
    loop = True
    while loop == True:
        if flip == 1:
            current_list = []
            matches = 0
            for each in possibility_list[begin_to_end_ix]:
                for key, values in full_dictionary.items():
                    if (each in values) and (each not in current_list):
                        current_list.append(key)
            # remove duplicates from current_list before inserting current_list into possiblity_list
            current_list = list(dict.fromkeys(current_list))
            #
            for current_val in current_list:
               for next_val in possibility_list[end_to_begin_ix]:
                   if next_val in mutate(current_val):
                       matches += 1
            if matches > 0:
                loop = False  
            # increment begin_to_end_ix before insert to ensure current list inserts into possiblity_list at appropriate place
            begin_to_end_ix += 1
            # insert current list into appropriate place in possibility_list
            possibility_list.insert(begin_to_end_ix, current_list)
        if flip == -1:
            current_list = []
            matches = 0
            for each in possibility_list[end_to_begin_ix]:
                for key, values in full_dictionary.items():
                    if (each in values) and (each not in current_list):
                        current_list.append(key)
            # remove duplicates from current_list before inserting current_list into possiblity_list
            current_list = list(dict.fromkeys(current_list))    
            #
            for current_val in current_list:
                for next_val in possibility_list[begin_to_end_ix]:
                    if next_val in mutate(current_val):
                        matches += 1
            if matches > 0:
                loop = False
                
            possibility_list.insert(end_to_begin_ix, current_list)
            # increment end_to_begin_ix after insert to ensure current list inserts into possibility_list at appropriate place
            end_to_begin_ix -= 1
        # start at opposite end of list next loop                   
        flip *= -1
    return possibility_list


def link(possibility_list):
    simplified = []
    first = possibility_list[0][0]
    last = possibility_list[-1][0]
    simplified.append(first)
    for num in range(1, len(possibility_list) - 1):
        for each in possibility_list[num]:
            mute_list = mutate(each)
            mute_set = set(mute_list)
            next_set = set(possibility_list[num + 1])
            previous_set = set(possibility_list[num - 1])
            next_match = mute_set & next_set
            previous_match = mute_set & previous_set
            if (each in next_match) and (each in previous_match):
                simplified.append(each)
                break
    simplified.append(last)
    return simplified
    
def build_chain(first, last):
    if len(first) == len(last) and first != last:
        build_data_structures(first, last)
    else:
        print('Word lengths do not match.')
        return
    possibilities = build_possibility_list(first, last)
    chain = link(possibilities)
    return chain


if __name__ == '__main__':
    if len(sys.argv) == 3:
        fw = str(sys.argv[1])
        lw = str(sys.argv[2])
    else:
        fw = input('Enter first word: ')
        lw = input('Enter last word: ')

    chain = build_chain(fw, lw)
    
    for num, each in enumerate(chain):
        print(num, each)
