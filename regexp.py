#!/usr/bin/python3

import re

file1 = input('Enter a file name: ')
srchstr = input('Enter a search string: ')

for line in open(file1):
    if re.search(srchstr, line):
        print(line, end='')
