#!/usr/bin/python3

list1 = []
list2 = []

user_choice = -1 
while user_choice != 0:
    print()
    print('User Menu')
    print()
    print('1. Add item(s) to a list')
    print('2. Remove an item from a list')
    print('3. Reverse a list')
    print('4. Display both lists')
    print('0. Quit')
    print()
    user_choice = int(input('What would you like to do? '))
    if user_choice == 1:
        user_item = input('Enter item(s) to add: ')
        which_list = int(input('Add it to list 1 or 2? '))
        if which_list == 1:
            items = user_item.split(' ')
            for item in items:
                list1.append(item)
            print(list1)
        elif which_list == 2:
            items = user_item.split(' ')
            for item in items:
                list2.append(item)
            print(list2)
        else:
            print('Invalid option. Nothing done.')
    elif user_choice == 2:
        user_item = input('Enter an item to remove from a list: ')
        which_list = int(input('Remove item from list 1 or 2? '))  
        if which_list == 1:
            try:
                user_item = int(user_item)
                list1.pop(user_item)
            except:
                list1.remove(user_item)
            print(list1)
        elif which_list == 2:
            try:
                user_item = int(user_item)
                list2.pop(user_item)
            except:
                list2.remove(user_item)
            print(list2)
        else:
            print('Invalid option. Nothing done.')
    elif user_choice == 3:
        user_input = int(input('Reverse list 1 or 2? '))
        if user_input == 1:
            list1 = list1[::-1]
            print(list1)
        elif user_input == 2:
            list2 = list2[::-1]
            print(list2)
        else:
            print('Invalid option. Nothing done.')
    elif user_choice == 4:
        print('list1 is', list1)
        print('list2 is', list2)
    elif user_choice == 0:
        continue
    else:
        print('Invalid option.')
