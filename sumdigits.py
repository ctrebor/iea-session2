#!/usr/bin/python3

def sumdigits(x):
    x = str(x)
    y = ''
    for char in x:
        if char == x[len(x) - 1]:
            y += str(char)
        else:
            y += str(char + '+')
    z = eval(y)
    print('z = ' + str(z))
    while z > 9:
        z = sumdigits(z)
    return z

number = int(input('Enter an integer: '))
print('sum = ' + str(sumdigits(number)))
