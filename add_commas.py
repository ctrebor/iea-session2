#!/usr/bin/python3

def add_commas(x):
    x = str(x)
    prefix = len(x) % 3
    y = ''
    for char in range(0, prefix):
        y += str(x[char])
    y += ','
    loop = 1
    for char in range(prefix, len(x)):
        if loop == len(x) - prefix:
            y += str(x[char])
        elif loop % 3 == 0:
            y += str(x[char] + ',')
        else:
            y += str(x[char])
        loop += 1
    return y

number = int(input('Enter an integer: '))
print(add_commas(number))
