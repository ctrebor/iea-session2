#!/usr/bin/python3

user_file = input('Enter a file to reverse: ')
with open(user_file, 'r') as f1:
    data = f1.readlines()

data2 = data[::-1]

with open('reverse.txt', 'w') as f2:
    f2.writelines(data2)
