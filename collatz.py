#!/usr/bin/python3

# Collatz Conjecture
def collatz(x):
    while x > 1:
        if x % 2 == 0:
            x = x // 2
        else:
            x = x * 3 + 1
        print('x = ' + str(x))
    return x

number = int(input('Enter an integer: '))
collatz(number)
