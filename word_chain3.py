#!/usr/bin/python3

solution = {}
matches = []
word_list = []

def build_word_list(length):
    with open ("words.full", "r") as data:
        for line in data:
            line = line.strip()
            if len(line) == length:
                word_list.append(line) 
    data.close()
    return

def mutate(first, last):
    mutations = []
    for word in word_list:
        index = 0
        char_match_first = 0
        char_match_last = 0
        if len(word) == len(first):
            for char in word:
                if char == first[index]:
                    char_match_first += 1
                if char == last[index]:
                    char_match_last += 1
                index += 1
            if char_match_first == len(word) - 1:
                if char_match_last >= 1:
                    if word not in mutations and word not in matches:
                        mutations.append(word)

    for each in mutations:
        if each not in matches:
            matches.append(each)
        else:
            mutations.remove(each)

    if mutations:
        solution[first] = mutations
    
    if last not in mutations:
        for each in mutations:
            mutate(each, last)

    return


fw = input('Enter the first word: ')
lw = input('Enter the last word: ')
if len(fw) == len(lw):
    build_word_list(len(fw))

chain = mutate(fw, lw)

for key, value in solution.items():
    print('key =', key, 'value =', value)
